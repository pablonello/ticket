<?php
$title = "Sobre Mi | ";
include "head.php";
include "sidebar.php";
?>

<div class="right_col" role="main"> <!-- page content -->
    <div class="">
        <div class="page-title">
            <br>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">

                    <h1 style="text-align: center;">TICKET</h1>
                    <h2>TICKET - <small>Sistema de Tickets de Soporte</small></h2>

                    <h3>Descripcion</h3>
                    <p>Ticket es un sistema para gestionar tickets de soporte usando proyectos, categorias, prioridades y mas, ideal para todo tipo de negocios, desarrollo de proyectos, gestion personal, etc, el sistema esta desarrollado con PHP y MySQL.</p>
                    <p>TICKET es un sistema de tickets de soporte en el que puedes rellenar tu perfil, agregar ticket, proyectos y categorias, generar reportes y mucho mas.</p>
                    <br>
                    <br>
                    <div  style="text-align: center;">
                        <a href="https:4topiso.com" target="_blank">
                            <img src="images/logo-cuarto-piso2.jpg" class="rounded" alt="...">
                        </a>
                    </div>
                </div>

            </div>
        </div>

    </div>

</div><!-- /page content -->


<?php include "footer.php" ?>
